﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MeshVertexEditor))]
public class MeshVertexInspector : Editor
{
    MeshVertexEditor _editor;

    bool _pickingMovementPoints = false;
    bool _pickingRigidPoints = false;

    Transform handleTransform;
    Quaternion handleRotation;

    Vector3[] _vertices;

    void OnSceneGUI()
    {
        _editor = target as MeshVertexEditor;
        DisplayEditMode();
    }

    void DisplayEditMode()
    {
        _vertices = _editor.GetMeshVertices();
        
        if ((_pickingMovementPoints)||(_pickingRigidPoints))
        {
            handleTransform = _editor.transform;
            handleRotation = Tools.pivotRotation == PivotRotation.Local ?
                handleTransform.rotation : Quaternion.identity;
            for (int i = 0; i < _vertices.Length; i++)
            {
                ShowPoint(i);
            }
        } else
        {
        }
    }

    private void ShowPoint(int index)
    {
        Vector3 point = handleTransform.TransformPoint(_vertices[index]);
        Handles.color = Color.blue;
        //point = Handles.FreeMoveHandle(point, handleRotation, _editor.handleSize,
        //        Vector3.zero, Handles.DotHandleCap);
        if (Handles.Button(point, handleRotation, _editor.handleSize, _editor.handleSize * 2, Handles.DotHandleCap))
        {
            Debug.Log("Button " + index + " was pressed");
            _editor.AddInteractionPoint(index);
        }

        if (GUI.changed) //3
        {
            
        }
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        _editor = target as MeshVertexEditor;

        GUILayout.BeginHorizontal();

        bool _tmpPickingMovement = _pickingMovementPoints;
        bool _tmpPickingRigid = _pickingRigidPoints;

        _pickingMovementPoints = GUILayout.Toggle(_tmpPickingMovement, "Edit Movement Points", "Button");
        _pickingRigidPoints = GUILayout.Toggle(_tmpPickingRigid, "Edit Rigid Points", "Button");

        if (_pickingMovementPoints != _tmpPickingMovement)
            if (_pickingMovementPoints)
                _pickingRigidPoints = false;

        if (_pickingRigidPoints != _tmpPickingRigid)
            if (_pickingRigidPoints)
                _pickingMovementPoints = false;

        DisplayEditMode();

        GUILayout.EndHorizontal();


        if (GUILayout.Button("Reset")) //1
        {
            _editor.Reset();
        }
    }
}
