﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    [SerializeField]
    float speed = 5;
    [SerializeField]
    Camera cam;

    public System.Action<bool> OnCameraMoved;
    bool moved = false;

    Vector3 oldPos;
    Vector3 oldCamPos;
    Quaternion oldRot;

    private void Awake()
    {
        if (cam == null)
            cam = Camera.main;
    }

    void Update()
    {
        oldPos = transform.position;
        oldCamPos = cam.transform.position;
        oldRot = transform.rotation;

        if (Input.GetKey(KeyCode.A))
            transform.Rotate(Vector3.up, speed * Time.deltaTime);

        if (Input.GetKey(KeyCode.D))
            transform.Rotate(Vector3.up, -speed * Time.deltaTime);

        if (Input.GetKey(KeyCode.W))
            cam.transform.position += cam.transform.forward * speed * Time.deltaTime;

        if (Input.GetKey(KeyCode.S))
            cam.transform.position -= cam.transform.forward * speed * Time.deltaTime;

        if (Input.GetKey(KeyCode.UpArrow))
            transform.position += Vector3.Cross(transform.forward, cam.transform.forward) * speed * Time.deltaTime;
        
        if (Input.GetKey(KeyCode.DownArrow))
            transform.position += Vector3.Cross(transform.forward, cam.transform.forward) * -speed * Time.deltaTime;

        if (Input.GetKey(KeyCode.RightArrow))
            transform.position += cam.transform.right * speed * Time.deltaTime;

        if (Input.GetKey(KeyCode.LeftArrow))
            transform.position += cam.transform.right * -speed * Time.deltaTime;

        if ((oldPos != transform.position)||(oldCamPos != cam.transform.position)||(oldRot != transform.rotation))
        {
            if (!moved)
            {
                moved = true;
                if (OnCameraMoved != null)
                    OnCameraMoved(moved);
            }
        } else if (moved)
        {
            moved = false;
            if (OnCameraMoved != null)
                OnCameraMoved(moved);
        }

    }
}
