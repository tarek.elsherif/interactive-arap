﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleRotate : MonoBehaviour
{
    private void Update()
    {
        this.transform.Rotate(Vector3.up, Time.deltaTime * 20f);
    }
}
