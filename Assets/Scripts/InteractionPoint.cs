﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button), typeof(Image))]
public class InteractionPoint : MonoBehaviour
{
    [HideInInspector]
    public int vertexIndex;
    [HideInInspector]
    public bool followPoint = true;
    [HideInInspector]
    public Vector3 pointToFollow;
    [HideInInspector]
    public MeshFilter mesh;
    [HideInInspector]
    public System.Action<int> onClickAction;

    [SerializeField]
    Image _image;
    [SerializeField]
    Button _button;
    [SerializeField]
    Text _text;

    void Awake()
    {
        if (_image == null)
            _image = GetComponent<Image>();
        if (_button == null)
            _button = GetComponent<Button>();
        if (_text == null)
            _text = GetComponentInChildren<Text>();
    }

    void OnEnable()
    {
        _button.onClick.AddListener(OnButtonClick);
    }

    void OnDisable()
    {
        _button.onClick.RemoveListener(OnButtonClick);
    }

    public void SetUp(int vertexIndex, MeshFilter mesh, Vector3 pointToFollow, System.Action<int> onClickAction, string title = "vertex", bool followPoint = true)
    {
        this.vertexIndex = vertexIndex;
        this.mesh = mesh;
        this.pointToFollow = pointToFollow;
        this.followPoint = followPoint;
        this.onClickAction = onClickAction;

        _text.text = title;
        _button.onClick.AddListener(OnButtonClick);
    }

    public void UpdatePosition(bool translated = false)
    {
        transform.position = GetVertexScreenPosition(pointToFollow, translated);
    }

    public void UpdatePosition(Vector3 newPos, bool translated = false)
    {
        transform.position = GetVertexScreenPosition(pointToFollow, translated);
    }

    public void DeactivateIfNotVisible(Camera camera, Vector3 normal)
    {
        if (Vector3.Dot(camera.transform.forward, normal) >= 0)
            gameObject.SetActive(false);
        else
            gameObject.SetActive(true);
    }

    public void OnButtonClick()
    {
        if (onClickAction != null)
            onClickAction(vertexIndex);
    }

    Vector2 GetVertexScreenPosition(Vector3 vertex, bool translated = true)
    {
        if (!translated)
            vertex = MeshVertexController.ConvertToRelativeTransform(mesh.transform, vertex);

        return Camera.main.WorldToScreenPoint(vertex);
    }
}
