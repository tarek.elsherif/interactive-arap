﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshVertexController))]
public class MeshAnimator : MonoBehaviour
{
    [SerializeField]
    MeshVertexController _controller;

    Vector3[] _vertices;

    void Awake()
    {
        if (_controller == null)
            _controller = GetComponent<MeshVertexController>();
    }

    void Start()
    {
        _vertices = _controller.Mesh.vertices;
    }

    void Update()
    {
        for (var i = 0; i < _vertices.Length; i++)
        {
            _vertices[i] = _controller.GetIntialVertices()[i] + (_controller.GetIntialNormals()[i] * (Mathf.Sin(Time.timeSinceLevelLoad) * 0.01f));
        }

        _controller.SetVertices(_vertices);
    }
}
