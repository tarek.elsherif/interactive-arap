﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public class MeshVertexController : MonoBehaviour
{
    [SerializeField]
    MeshFilter _meshFilter = null;
    [SerializeField]
    bool _showVertexGizmos = false;

    Mesh _originalMesh;
    Mesh _clonedMesh;
    
    Vector3[] _originalVertices;
    Vector3[] _originalNormals;

    Vector3[] _vertices;
    int[] _triangles;
    bool _isCloned = false;

    [HideInInspector]
    public List<int> movementHandlesIndices;
    [HideInInspector]
    public List<int> rigidPointsIndices;

    public Mesh Mesh
    {
        get { return _meshFilter.mesh; }
    }

    void Awake()
    {
        if (_meshFilter == null)
            _meshFilter = GetComponent<MeshFilter>();
        
        _meshFilter = GetComponent<MeshFilter>();
        _originalMesh = _meshFilter.sharedMesh; 
        _clonedMesh = new Mesh();

        _clonedMesh.name = "clone";
        _clonedMesh.vertices = _originalMesh.vertices;
        _clonedMesh.triangles = _originalMesh.triangles;
        _clonedMesh.normals = _originalMesh.normals;
        _clonedMesh.uv = _originalMesh.uv;
        _meshFilter.mesh = _clonedMesh;

        _vertices = _clonedMesh.vertices;
        _triangles = _clonedMesh.triangles;
        _isCloned = true;
        
        Debug.Log("[MeshVertexController] mesh: " + _meshFilter.transform.name + ", vertices count: " + _originalMesh.vertices.Length + ", triangles count: " + _originalMesh.triangles.Length);
    }

    void OnDrawGizmosSelected()
    {
        if (_showVertexGizmos)
        {
            if (_meshFilter == null)
                _meshFilter = GetComponent<MeshFilter>();

            Mesh mesh = _meshFilter.mesh;

            foreach (Vector3 vertex in mesh.vertices)
            {
                Gizmos.color = new Color(1, 0, 0, 0.5f);
                Vector3 vertexPos = new Vector3(vertex.x * transform.localScale.x, vertex.y * transform.localScale.y, vertex.z * transform.localScale.z);
                Vector3 gizmoPointPos = (transform.rotation * vertexPos) + transform.position;
                Gizmos.DrawSphere(gizmoPointPos, 0.025f);
            }
        }
    }

    public void ResetVertices()
    {
        _meshFilter.mesh.vertices = _originalVertices;
        _meshFilter.mesh.RecalculateBounds();
    }

    public void SetMesh(Mesh newMesh)
    {
        _meshFilter.mesh = newMesh;
        _vertices = _meshFilter.mesh.vertices;
    }

    public void SetVertices(Vector3[] newVertices)
    {
        Debug.Assert(newVertices.Length == _meshFilter.mesh.vertices.Length);
        _meshFilter.mesh.vertices = newVertices;
        _meshFilter.mesh.RecalculateBounds();
    }

    public void SetTriangles(int[] newTriangles)
    {
        _meshFilter.mesh.triangles = newTriangles;
    }

    public List<float> GetMovementHandlesPositions()
    {
        List<float> result = new List<float>(movementHandlesIndices.Count * 4);
        for (int i = 0; i < movementHandlesIndices.Count; i++)
        {
            result[i * 4] = movementHandlesIndices[i];
            result[i * 4 + 1] = _vertices[movementHandlesIndices[i]].x;
            result[i * 4 + 2] = _vertices[movementHandlesIndices[i]].y;
            result[i * 4 + 3] = _vertices[movementHandlesIndices[i]].z;
        }
        return result;
    }

    public List<float> GetRigidPointPositions()
    {
        List<float> result = new List<float>();
        for (int i = 0; i < rigidPointsIndices.Count; i++)
        {
            result.Add(rigidPointsIndices[i]);
            result.Add(_vertices[rigidPointsIndices[i]].x);
            result.Add(_vertices[rigidPointsIndices[i]].y);
            result.Add(_vertices[rigidPointsIndices[i]].z);
        }
        Debug.Assert(result.Count == rigidPointsIndices.Count * 4);
        return result;
    }

    public Vector3[] CovnertFloatArrayToVectorArray(float[] input)
    {
        Vector3[] output = new Vector3[input.Length / 3];
        for(int i = 0; i < output.Length; i++)
        {
            output[i] = new Vector3(input[i * 3], input[i * 3 + 1], input[i * 3 + 2]);
        }

        return output;
    }

    public Vector3[] CovnertFloatArrayToVectorArray(double[] input)
    {
        Vector3[] output = new Vector3[input.Length / 3];
        for (int i = 0; i < output.Length; i++)
        {
            output[i] = new Vector3((float)input[i * 3], (float)input[i * 3 + 1], (float)input[i * 3 + 2]);
        }

        return output;
    }

    public void SetVertex(int vertexIndex, Vector3 newPosition, bool translated = false)
    {
        IntPtr rigidPointsToPass = Marshal.AllocHGlobal(sizeof(float) * rigidPointsIndices.Count);
        Marshal.Copy(rigidPointsIndices.ToArray(), 0, rigidPointsToPass, rigidPointsIndices.Count);
        MeshGenerator.SetConstraint(rigidPointsIndices.Count, rigidPointsToPass);

        List<float> constraints = new List<float>();
        newPosition = (!translated) ? ConvertToMeshTransform(transform, newPosition) : newPosition;
        constraints.Add(vertexIndex);
        constraints.Add(newPosition.x);
        constraints.Add(newPosition.y);
        constraints.Add(newPosition.z);
        
        IntPtr newPositionPointsToPass = Marshal.AllocHGlobal(sizeof(float) * constraints.Count);
        Marshal.Copy(constraints.ToArray(), 0, newPositionPointsToPass, constraints.Count);
        MeshGenerator.SetPositions(constraints.Count, newPositionPointsToPass);
        MeshGenerator.Process();

        int verticesSize = MeshGenerator.GetDeformedVerticesSize() * 3;
        double[] vertices = new double[verticesSize];
        Marshal.Copy(MeshGenerator.GetDeformedVertices(), vertices, 0, verticesSize);

        int indicesSize = MeshGenerator.GetDeformedIndicesSize() * 3;
        Debug.Log(indicesSize);
        int[] indices = new int[indicesSize];
        Marshal.Copy(MeshGenerator.GetDeformedIndices(), indices, 0, indicesSize);

        SetVertices(CovnertFloatArrayToVectorArray(vertices));
        SetTriangles(indices);
    }

    public void SetMultipleVertices(int[] vertexIndices, Vector3[] newPositions, bool translated = false)
    {
        IntPtr rigidPointsToPass = Marshal.AllocHGlobal(rigidPointsIndices.Count);
        Marshal.Copy(rigidPointsIndices.ToArray(), 0, rigidPointsToPass, rigidPointsIndices.Count);
        MeshGenerator.SetConstraint(rigidPointsIndices.Count, rigidPointsToPass);

        Debug.Assert(vertexIndices.Length == newPositions.Length);
        List<float> constraints = new List<float>();
        for(int i = 0; i < vertexIndices.Length; i++)
        {
            Vector3 vector = (!translated) ? ConvertToMeshTransform(transform, newPositions[i]) : newPositions[i];
            constraints.Add(vertexIndices[i]);
            constraints.Add(vector.x);
            constraints.Add(vector.y);
            constraints.Add(vector.z);
        }

        IntPtr newPositionPointsToPass = Marshal.AllocHGlobal(constraints.Count);
        Marshal.Copy(constraints.ToArray(), 0, newPositionPointsToPass, constraints.Count);
        MeshGenerator.SetPositions(constraints.Count, newPositionPointsToPass);
        MeshGenerator.Process();

        int verticesSize = MeshGenerator.GetDeformedVerticesSize()*3;
        float[] vertices = new float[verticesSize];
        Marshal.Copy(MeshGenerator.GetDeformedVertices(), vertices, 0, verticesSize);

        int indicesSize = MeshGenerator.GetDeformedIndicesSize()*3;
        int[] indices = new int[indicesSize];
        Marshal.Copy(MeshGenerator.GetDeformedIndices(), indices, 0, indicesSize);

        SetVertices(CovnertFloatArrayToVectorArray(vertices));
        SetTriangles(indices);
    }

    public Vector3[] GetVertices()
    {
        return _meshFilter.mesh.vertices;
    }

    public Vector3[] GetIntialVertices()
    {
        return _originalMesh.vertices;
    }

    public Vector3[] GetIntialNormals()
    {
        return _originalMesh.normals;
    }

    public static Vector3 ConvertToRelativeTransform(Transform trans, Vector3 input)
    {
        Vector3 scale = trans.localScale;
        Vector3 pos = new Vector3(input.x * scale.x, input.y * scale.y, input.z * scale.z);
        return (trans.rotation * pos) + trans.position;
    }

    public static Vector3 ConvertToMeshTransform(Transform trans, Vector3 input)
    {
        Vector3 scale = trans.localScale;
        Vector3 pos = Quaternion.Inverse(trans.rotation) * (input - trans.position);
        pos = new Vector3(pos.x / scale.x, pos.y / scale.y, pos.z / scale.z);
        return pos;
    }
}
