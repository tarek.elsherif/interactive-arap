﻿using System.Collections.Generic;
using UnityEngine;

public class MovementHandle : HandlePoint
{
    [HideInInspector]
    public MeshVertexController controller;

    private Vector3 initialPosition;
    
    private Vector3 mOffset;
    private float mZCoord;

    public static List<MovementHandle> handles = new List<MovementHandle>();
    public static bool moveOnMouseUp = false;

    void Awake()
    {
        if (_collider == null)
            _collider = GetComponentInChildren<SphereCollider>();

        handles.Add(this);
        initialPosition = transform.position;
    }

    void OnDestroy()
    {
        handles.Remove(this);
    }

    public void movePoint(Vector3 pos)
    {
        if (controller == null)
        {
            Debug.LogWarning("[MovementHandle] MeshVertexController was not set.");
            controller = GameObject.FindObjectOfType<MeshVertexController>();
        }
        controller.SetVertex(vertexIndex, pos, false);
        UpdateAllHandlesPositions();
    }

    public void UpdatePosition()
    {
        if (controller == null)
        {
            Debug.LogWarning("[MovementHandle] MeshVertexController was not set.");
            controller = GameObject.FindObjectOfType<MeshVertexController>();
        }
        transform.position = MeshVertexController.ConvertToRelativeTransform(controller.transform, controller.Mesh.vertices[vertexIndex]);
    }

    void OnMouseDown()
    {
        mZCoord = Camera.main.WorldToScreenPoint(gameObject.transform.position).z;
        mOffset = gameObject.transform.position - GetMouseAsWorldPoint();
        initialPosition = transform.position;
    }

    void OnMouseDrag()
    {
        transform.position = GetMouseAsWorldPoint() + mOffset;
        if (!moveOnMouseUp)
            movePoint(transform.position);
    }

    void OnMouseUp()
    {
        //MoveAllHandles(transform.position - initialPosition);
        if (moveOnMouseUp)
            movePoint(transform.position);
    }

    private Vector3 GetMouseAsWorldPoint()
    {
        Vector3 mousePoint = Input.mousePosition;
        mousePoint.z = mZCoord;
        return Camera.main.ScreenToWorldPoint(mousePoint);
    }

    public static void UpdateAllHandlesPositions()
    {
        foreach (MovementHandle mh in handles)
        {
            mh.UpdatePosition();
        }
    }

    public static void MoveAllHandles(Vector3 transformation, int movedIndex = -1)
    {
        foreach (MovementHandle mh in handles)
        {
            if (mh.vertexIndex != movedIndex)
                mh.transform.position += transformation;
        }
    }
}
