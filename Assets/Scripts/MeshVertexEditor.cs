﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshVertexController), typeof(MeshFilter))]
[ExecuteInEditMode]
public class MeshVertexEditor : MonoBehaviour
{
    [SerializeField]
    MeshVertexController _controller;
    [SerializeField]
    MeshFilter _meshFilter;
    [SerializeField]
    GameObject _interactionPointPrefab;
    [SerializeField]
    GameObject _interactionPointsParent;
    
    List<GameObject> _interactionPoints;
    List<GameObject> _rigidPoints;

    [HideInInspector]
    public Vector3[] vertices;

    public float handleSize = 0.03f;

    private void Start()
    {
        if (_controller == null)
            _controller = GetComponent<MeshVertexController>();
        if (_meshFilter == null)
            _meshFilter = GetComponent<MeshFilter>();
        if (_interactionPointsParent == null)
            _interactionPointsParent = this.gameObject;

        vertices = _meshFilter.mesh.vertices;
        _interactionPoints = new List<GameObject>();
        InitializeInteractionPoints(vertices);

        Debug.Log(vertices.Length);
    }

    void InitializeInteractionPoints(Vector3[] vertices)
    {
        _interactionPoints = new List<GameObject>();
        //foreach(Vector3 vertex in vertices)
        //{
        //    GameObject interactionPoint = Instantiate(_interactionPointPrefab, _interactionPointsParent.transform);
        //    Vector3 vertexPos = new Vector3(vertex.x * transform.localScale.x, vertex.y * transform.localScale.y, vertex.z * transform.localScale.z);
        //    interactionPoint.transform.position = (transform.rotation * vertexPos) + transform.position;
        //    _interactionPoints.Add(interactionPoint);
        //}
    }

    public Vector3[] GetMeshVertices()
    {
        return _meshFilter.mesh.vertices;
    }

    public void AddInteractionPoint(int index)
    {
        GameObject iPoint = Instantiate(_interactionPointPrefab, _interactionPointsParent.transform);
        iPoint.transform.position = ConvertToRelativeTransform(_meshFilter.mesh.vertices[index]);
        _interactionPoints.Add(iPoint);
    }

    Vector3 ConvertToRelativeTransform(Vector3 input)
    {
        Vector3 scale = transform.localScale;
        Vector3 pos = new Vector3(input.x * scale.x, input.y * scale.y, input.z * scale.z);
        return (transform.rotation * pos) + transform.position;
    }

    public void Reset()
    {
        foreach(GameObject go in _interactionPoints)
        {
            Destroy(go);
        }
        _interactionPoints.Clear();
    }
}
