﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControllerUI : MonoBehaviour
{
    public static ControllerUI instance = null;

    [SerializeField]
    MeshVertexRunTimeEditor _editor;

    [SerializeField]
    GameObject _meshPrefab;
    [SerializeField]
    Rotator _camController;

    [Header("UI References:")]
    [SerializeField]
    Button _movementHandlesButton;
    [SerializeField]
    Button _rigidPointsButton;
    [SerializeField]
    Button _resetButton;
    [SerializeField]
    Button _mouseEventButton;
    [SerializeField]
    Button _debugButton;
    [SerializeField]
    GameObject _loadingOverlay;
    [SerializeField]
    GameObject _debugText;

    EditorState _oldEditorState = EditorState.None;
    bool _showDebug = false;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);
    }

    void Start()
    {
        UpdateUI();
        _camController.OnCameraMoved += (moved) => {
            if (moved)
            {
                _oldEditorState = _editor.currentEditorState;
                _editor.SetEditorState(EditorState.None);
                UpdateUI();
            } else
            {
                _editor.SetEditorState(_oldEditorState);
                UpdateUI();
            }
        };
    }

    public bool FindAndSetEditor()
    {
        if (_editor == null)
        {
            Debug.LogWarning("[ControllerUI] MeshVertexRunTimeEditor was not set.");
            _editor = GameObject.FindObjectOfType<MeshVertexRunTimeEditor>();
        }
        return (_editor != null);
    }

    public void SetEditor(MeshVertexRunTimeEditor editor)
    {
        _editor = editor;
    }

    public void UpdateUI()
    {
        if (FindAndSetEditor())
        {
            switch (_editor.currentEditorState)
            {
                case (EditorState.None):
                    _movementHandlesButton.image.color = Color.white;
                    _rigidPointsButton.image.color = Color.white;
                    _editor.enabled = false;
                    break;
                case (EditorState.MovementHandles):
                    _movementHandlesButton.image.color = Color.grey;
                    _rigidPointsButton.image.color = Color.white;
                    _editor.enabled = true;
                    break;
                case (EditorState.RigidPoints):
                    _movementHandlesButton.image.color = Color.white;
                    _rigidPointsButton.image.color = Color.grey;
                    _editor.enabled = true;
                    break;
                default:
                    _movementHandlesButton.image.color = Color.white;
                    _rigidPointsButton.image.color = Color.white;
                    _editor.enabled = false;
                    break;
            }
        }

        if (_showDebug)
        {
            _debugButton.image.color = Color.grey;
        } else
        {
            _debugButton.image.color = Color.white;
        }

        if (MovementHandle.moveOnMouseUp)
        {
            _mouseEventButton.image.color = Color.grey;
        } else
        {
            _mouseEventButton.image.color = Color.white;
        }
    }

    public void MovementHandlesButtonPressed()
    {
        if (!FindAndSetEditor())
            return;

        if (_editor.currentEditorState == EditorState.MovementHandles)
            SetEditorState(EditorState.None);
        else
            SetEditorState(EditorState.MovementHandles);
        
        StartCoroutine(ShowLoadingThenDoActionCoroutine(UpdateUI));
    }

    public void RigidPointsButtonPressed()
    {
        if (!FindAndSetEditor())
            return;

        if (_editor.currentEditorState == EditorState.RigidPoints)
            SetEditorState(EditorState.None);
        else
            SetEditorState(EditorState.RigidPoints);

        StartCoroutine(ShowLoadingThenDoActionCoroutine(UpdateUI));
    }

    public Tuple<Vector3[], int[]> GetMeshVerticesIndeices(int modelIdx)
    {
        int verticesSize = MeshGenerator.GetVerticesSize(modelIdx)*3;
        double[] vertices = new double[verticesSize];
        Marshal.Copy(MeshGenerator.GetVertices(modelIdx), vertices, 0, verticesSize);

        int indicesSize = MeshGenerator.GetIndicesSize(modelIdx)*3;
        int[] indices = new int[indicesSize];
        Marshal.Copy(MeshGenerator.GetIndices(modelIdx), indices, 0, indicesSize);

        var meshVertices = new Vector3[verticesSize / 3];
        for (int i = 0; i < vertices.Length; i += 3)
            meshVertices[i / 3] = new Vector3((float)vertices[i], (float)vertices[i + 1], (float)vertices[i + 2]);

        return Tuple.Create<Vector3[], int[]>(meshVertices, indices);
    }

    public void LoadMesh(int modelIdx)
    {
        if (_editor != null)
            Destroy(_editor.gameObject);

        Tuple<Vector3[], int[]> meshVI = GetMeshVerticesIndeices(modelIdx);
        GameObject meshGO = Instantiate(_meshPrefab);

        MeshFilter meshFilter = meshGO.GetComponent<MeshFilter>();
        MeshVertexController controller = meshGO.GetComponent<MeshVertexController>();
        MeshVertexRunTimeEditor editor = meshGO.GetComponent<MeshVertexRunTimeEditor>();
        _editor = editor;

        Mesh mesh = new Mesh();
        mesh.Clear();
        mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
        mesh.vertices = meshVI.Item1;
        mesh.triangles = meshVI.Item2;

        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
        mesh.RecalculateTangents();

        MeshGenerator.SetBaseMesh(modelIdx);

        controller.SetMesh(mesh);
        _editor.Initialize();
    }

    public void GenerateMesh(int modelIdx)
    {
        ControllerUI.instance.ShowLoadingThenDoAction(() => { LoadMesh(modelIdx); });
    }

    public void ResetButtonPressed()
    {
        if (!FindAndSetEditor())
            return;

        _editor.Reset();
    }

    public void DebugButtonPressed()
    {
        _showDebug = !_showDebug;
        _debugText.SetActive(_showDebug);
        UpdateUI();
    }

    public void MouseEventButtonPressed()
    {
        MovementHandle.moveOnMouseUp = !MovementHandle.moveOnMouseUp;
        UpdateUI();
    }

    public void RestartButtonPressed()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    public void ExitButtonPressed()
    {
        Application.Quit();
    }

    public void ShowLoadingThenDoAction(System.Action action)
    {
        StartCoroutine(ShowLoadingThenDoActionCoroutine(action));
    }

    IEnumerator ShowLoadingThenDoActionCoroutine(System.Action action)
    {
        ShowLoadingPanel(true);
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        action();
        ShowLoadingPanel(false);
    }

    public void ShowLoadingPanel(bool show)
    {
        _loadingOverlay.SetActive(show);
    }

    public void SetEditorState(EditorState newState)
    {
        _editor.SetEditorState(newState);
    }
}
