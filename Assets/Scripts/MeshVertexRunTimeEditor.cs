﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshVertexRunTimeEditor : MonoBehaviour
{
    [Header("Component References:")]
    [SerializeField]
    MeshVertexController _controller;
    [SerializeField]
    MeshFilter _meshFilter;
    [SerializeField]
    GameObject _interactionPointsParent;

    [Header("Prefab References:")]
    [SerializeField]
    GameObject _interactionPointPrefab;
    [SerializeField]
    GameObject _movementHandlePrefab;
    [SerializeField]
    GameObject _rigidPointPrefab;

    [Header("Settings:")]
    public float interactionPointSize = 0.03f;
    public float handleSize = 0.25f;
    
    [HideInInspector]
    public EditorState currentEditorState = EditorState.None;

    List<InteractionPoint> _interactionPoints = null;
    List<MovementHandle> _movementHandles = null;
    List<HandlePoint> _rigidPoints = null;

    void OnEnable()
    {
        if (_interactionPoints == null)
            return;
        UpdateInteractionPointsUI();
    }

    void OnDisable()
    {
        if (_interactionPoints == null)
            return;
        foreach (InteractionPoint ip in _interactionPoints)
            ip.gameObject.SetActive(false);
    }

    void OnDestroy()
    {
        if (_interactionPoints == null)
            return;
        foreach (InteractionPoint ip in _interactionPoints)
            Destroy(ip.gameObject);
    }

    void InitializeInteractionPoints(Vector3[] vertices)
    {
        int index = 0;
        foreach (Vector3 vertex in vertices)
        {
            InteractionPoint interactionPoint = Instantiate(_interactionPointPrefab, _interactionPointsParent.transform).GetComponent<InteractionPoint>();
            interactionPoint.SetUp(index, _meshFilter, vertex, ToggleInteractionPoint, index.ToString(), false);
            _interactionPoints.Add(interactionPoint);
            ++index;
            interactionPoint.gameObject.SetActive(false);
        }
        //UpdateInteractionPointsUI();
    }

    public void Initialize()
    {
        if (_controller == null)
            _controller = GetComponent<MeshVertexController>();
        if (_meshFilter == null)
            _meshFilter = GetComponent<MeshFilter>();
        if (_interactionPointsParent == null)
            _interactionPointsParent = FindObjectOfType<ControllerUI>().transform.GetChild(0).gameObject;
        
        _movementHandles = new List<MovementHandle>();
        _rigidPoints = new List<HandlePoint>();
        _interactionPoints = new List<InteractionPoint>();

        ControllerUI.instance.ShowLoadingThenDoAction(() => { InitializeInteractionPoints(_meshFilter.mesh.vertices); });
    }

    public void UpdateInteractionPointsUI()
    {
        Vector3[] vertices = _meshFilter.mesh.vertices;
        Vector3[] normals = _meshFilter.mesh.normals;
        foreach (InteractionPoint ip in _interactionPoints)
        {
            ip.pointToFollow = vertices[ip.vertexIndex];
            ip.UpdatePosition();
            ip.DeactivateIfNotVisible(Camera.main, normals[ip.vertexIndex]);
        }
    }

    public Vector3[] GetMeshVertices()
    {
        return _meshFilter.mesh.vertices;
    }

    public void ToggleInteractionPoint(int index)
    {
        switch (currentEditorState)
        {
            case (EditorState.MovementHandles):

                if (_controller.movementHandlesIndices.Contains(index))
                {
                    // TODO: Remove movement handle
                    return;
                }

                MovementHandle movementHandle = Instantiate(_movementHandlePrefab, transform).GetComponent<MovementHandle>();
                movementHandle.vertexIndex = index;
                movementHandle.controller = _controller;
                movementHandle.transform.position = MeshVertexController.ConvertToRelativeTransform(this.transform, _meshFilter.mesh.vertices[index]);

                _movementHandles.Add(movementHandle);
                _controller.movementHandlesIndices.Add(index);
                break;
            case (EditorState.RigidPoints):

                if (_controller.rigidPointsIndices.Contains(index))
                {
                    // TODO: Remove rigid point
                    return;
                }

                HandlePoint rigidPoint = Instantiate(_rigidPointPrefab, transform).GetComponent<HandlePoint>();
                rigidPoint.vertexIndex = index;
                rigidPoint.transform.position = MeshVertexController.ConvertToRelativeTransform(this.transform, _meshFilter.mesh.vertices[index]);

                _rigidPoints.Add(rigidPoint);
                _controller.rigidPointsIndices.Add(index);
                break;
            default:
                break;
        }
    }

    public void RemoveMovementHandle(GameObject movementHandle)
    {
        throw new NotImplementedException();
    }

    public void SetEditorState(EditorState editorState)
    {
        currentEditorState = editorState;
    }

    public void Reset()
    {
        foreach (MovementHandle mh in _movementHandles)
        {
            Destroy(mh.gameObject);
        }
        foreach (HandlePoint rp in _rigidPoints)
        {
            Destroy(rp.gameObject);
        }
        _movementHandles.Clear();
        _rigidPoints.Clear();
        _controller.movementHandlesIndices.Clear();
        _controller.rigidPointsIndices.Clear();
    }
}

public enum EditorState
{
    None,
    MovementHandles,
    RigidPoints
}
