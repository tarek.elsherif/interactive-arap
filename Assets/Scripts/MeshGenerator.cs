﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class MeshGenerator : MonoBehaviour
{
    [DllImport("InteractiveARAP", CallingConvention = CallingConvention.Cdecl)]
    public static extern void Initialize();

    [DllImport("InteractiveARAP", CallingConvention = CallingConvention.Cdecl)]
    public static extern IntPtr GetVertices(int modelIndex);
    
    [DllImport("InteractiveARAP", CallingConvention = CallingConvention.Cdecl)]
    public static extern int GetVerticesSize(int modelIndex);
    
    [DllImport("InteractiveARAP", CallingConvention = CallingConvention.Cdecl)]
    public static extern IntPtr GetIndices(int modelIndex);
    
    [DllImport("InteractiveARAP", CallingConvention = CallingConvention.Cdecl)]
    public static extern int GetIndicesSize(int modelIndex);
    
    [DllImport("InteractiveARAP", CallingConvention = CallingConvention.Cdecl)]
    public static extern IntPtr GetDeformedVertices();
    
    [DllImport("InteractiveARAP", CallingConvention = CallingConvention.Cdecl)]
    public static extern int GetDeformedVerticesSize();
    
    [DllImport("InteractiveARAP", CallingConvention = CallingConvention.Cdecl)]
    public static extern IntPtr GetDeformedIndices();
    
    [DllImport("InteractiveARAP", CallingConvention = CallingConvention.Cdecl)]
    public static extern int GetDeformedIndicesSize();

    [DllImport("InteractiveARAP", CallingConvention = CallingConvention.Cdecl)]
    public static extern void SetBaseMesh(int meshIdx);

    [DllImport("InteractiveARAP", CallingConvention = CallingConvention.Cdecl)]
    public static extern void SetConstraint(int size, IntPtr constraints);
    
    [DllImport("InteractiveARAP", CallingConvention = CallingConvention.Cdecl)]
    public static extern void SetPositions(int size, IntPtr constraints);
    
    [DllImport("InteractiveARAP", CallingConvention = CallingConvention.Cdecl)]
    public static extern void Process();

    GameObject obj;
    public Material material;
    int modelIdx = 0;
    int keyPresses = 0;

    // Start is called before the first frame update
    void Start()
    {
        Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            modelIdx = 0;
            CreateMesh();
        }

        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            modelIdx = 5;
            CreateMesh();
        }

        if (Input.GetKeyDown(KeyCode.M))
            ProcessMesh();
    }

    public void CreateMesh()
    {
        int verticesSize = GetVerticesSize(modelIdx);
        float[] vertices = new float[verticesSize];
        Marshal.Copy(GetVertices(modelIdx), vertices, 0, verticesSize);

        int indicesSize = GetIndicesSize(modelIdx);
        int[] indices =  new int[indicesSize];
        Marshal.Copy(GetIndices(modelIdx), indices, 0, indicesSize);

        obj = new GameObject("rendering object");
        MeshFilter filter = obj.AddComponent<MeshFilter>();
        MeshRenderer renderer = obj.AddComponent<MeshRenderer>();

        Mesh mesh = new Mesh();
        mesh.Clear();
        mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;

        var meshvertices = new Vector3[verticesSize / 3];
        for (int i = 0; i < vertices.Length; i += 3)
            meshvertices[i / 3] = new Vector3(vertices[i], vertices[i + 1], vertices[i + 2]);

        mesh.vertices = meshvertices;
        mesh.triangles = indices;
        //mesh.RecalculateNormals();
        //mesh.RecalculateBounds();
        //mesh.normals = meshvertices;
        filter.mesh = mesh;
        renderer.sharedMaterial = material;
        Vector3 center = renderer.bounds.center;
        Transform camloc = Camera.main.transform;
        obj.transform.position = camloc.position + center + camloc.forward * 1.5f;
        obj.transform.localScale /= renderer.bounds.size.magnitude;
    }

    public void ProcessMesh()
    {
        SetBaseMesh(modelIdx);
        System.Random rnd= new System.Random();
        List<int> handlevertices = new List<int>();
        List<double> pos = new List<double>();

        System.Diagnostics.Stopwatch timewatch = new System.Diagnostics.Stopwatch();
        timewatch.Start();
        while (handlevertices.Count < 5)
        {
            int qq = rnd.Next(100);
            bool pass = false;
            foreach (int x in handlevertices)
            {
                if (x == qq)
                {
                    pass = true;
                    break;
                }
            }
            if(!pass)
            {
                handlevertices.Add(qq);
                pos.Add(rnd.NextDouble());
                pos.Add(rnd.NextDouble());
                pos.Add(rnd.NextDouble());
            }
        }
        
        float[] data = new float[20];
        for(int i=0; i< 5; i++)
        {
            data[i * 4] = handlevertices[i];
            data[i * 4 + 1] = (float)pos[i*3];
            data[i * 4 + 2] = (float)pos[i*3 + 1];
            data[i * 4 + 3] = (float)pos[i*3 + 2];
        }

        IntPtr datatopass = Marshal.AllocHGlobal(20);
        Marshal.Copy(data, 0, datatopass, 20);
        Debug.Log(timewatch.ElapsedMilliseconds);
        timewatch.Restart();
        Process();
        Debug.Log(timewatch.ElapsedMilliseconds);

        timewatch.Restart();
        int verticesSize = GetDeformedVerticesSize();
        float[] vertices = new float[verticesSize];
        Marshal.Copy(GetDeformedVertices(), vertices, 0, verticesSize);

        int indicesSize = GetDeformedIndicesSize();
        int[] indices = new int[indicesSize];
        Marshal.Copy(GetDeformedIndices(), indices, 0, indicesSize);

        MeshFilter filter = obj.GetComponent<MeshFilter>();
        MeshRenderer renderer = obj.GetComponent<MeshRenderer>();

        Mesh mesh = new Mesh();
        mesh.Clear();
        mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;

        var meshvertices = new Vector3[verticesSize / 3];
        for (int i = 0; i < vertices.Length; i += 3)
            meshvertices[i / 3] = new Vector3(vertices[i], vertices[i + 1], vertices[i + 2]);

        mesh.vertices = meshvertices;
        mesh.triangles = indices;
        filter.mesh = mesh;
        Debug.Log(timewatch.ElapsedMilliseconds);
    }
}
