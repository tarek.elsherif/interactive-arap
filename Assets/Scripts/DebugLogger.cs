﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugLogger : MonoBehaviour
{
    public Text textbox;
    // Start is called before the first frame update
    void Start()
    {
        Application.logMessageReceived += Logging;
    }

    public void Logging(string message,string stackTrace, LogType type)
    {
        textbox.text = message + "\n" + textbox.text;
    }

    private void OnDestroy()
    {
        Application.logMessageReceived -= Logging;
    }
}
