﻿using UnityEngine;

public abstract class HandlePoint : MonoBehaviour
{
    [HideInInspector]
    public int vertexIndex;

    [SerializeField]
    protected SphereCollider _collider;
}
